# Ansible repository for ELK stack deployment for Ubuntu stack

## 1. Intro

This playbook installs full ELK stack with filebeat on Ubuntu x64 systems. Following machines were used:

* 3x Elasticsearch nodes with Ubuntu 19.10 x64 | 2GB Memory | 1 CPU | 50 GB SSD
* 1x Kibana node with Ubuntu 19.10 x64 | 2GB Memory | 50 GB SSD
* 1x Logstash node with Ubuntu 19.10 x64 | 2GB Memory | 50 GB SSD

*Note that node count can be increased but some manual steps are needed when doing so*

## 2. Playbooks
* java - *will add repos and install pre-requisites like jdk-11. Is included to the main playbook*
* deploy_elk_stack - *will install elk stack and configure it*

## 3. Repo Structure

### 3.1 Roles
There is seperate role for each service/application and one role for pre-requisites install. All relevant configs of services are undrel /role/templates. Most of the templates use variables that are all defined under /hsots/inventory/group_vars/all/main.yaml.

### 3.2 Inventroy (Hosts info)
All hosts are defined in inventory/hosts file. Hosts are group by services.

### 3.3 Configuration templates:
All service configuration files that need to be updated are included to roles/templates. Most of the config files are using variables that are defined under /hsots/inventory/group_vars/all/main.yaml

## 4. Requirements and changes before running the playbook

* Edit the inventory/hosts file to match your cluster settings
* Make sure following network rules are done (ports opened)
    * 9200 and 9300 ports should be open between Elasticsearch nodes and elastic nodes should be also reachable from those port for logstash and kibana nodes
    * Logstash is listening on port 5042 (defined in variables) so this port should be opened so logstash node would be reachable from that port
    * Kibana is available from 5061 so opening this port to access kibana is nessecary too
    * General 80 port for kibana or 443 if you decide to implement SSL solution

## 5. Running the playbook

To run the full playbook, use the following command in your repo dir:

```
ansible-playbook deploy_elk_stack.yaml -i inventory/host
```

Tags can also be used with playbook command to shorten the proccess of playbook. Available tags:
* Elasticsearch
* Kibana
* Logstash
* Prerequisites

## 6. Tasks after playbook has ran succesfully

* Create admin user for Kibana - log in to kibana node, switch yourself to root and run the follwoing:
```
echo "kibanaadmin:`openssl passwd -apr1`" | sudo tee -a /etc/nginx/htpasswd.users
```
## 7. Scalability

More elasticsearch nodes can be always added to the cluster. Following steps are needed to make it work

* Add nodes to inventory/hosts file
* Add new hosts also to variables file (inventory/group_vars/all/main.yml)
* Edit elasticsearch template with new creted variables to update cluster info
* Optional: Add relevant new hosts to filebeat template config as well
* Run the playbook (can use --tags when running playbook to shorten the proccess)

## 8. Notes

* For bigger clusters more automated and easy to use solution for inventory files can be used.
* Do note that all the nodes are assigned 1024m of heap memory from group_vars, this can be changed as well for all hosts or each host individually by defining it under host vars if needed.

